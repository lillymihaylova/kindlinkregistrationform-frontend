import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Organization } from '../model/organization';

import { Router } from '@angular/router';
import { OrganizationService } from '../services/organization.service';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'registration-form',
  templateUrl: 'app/components/registration-form.component.html'
})
export class RegistrationFormComponent{
        
   
    active = true;
    submitted = false;

    constructor(private organizationService: OrganizationService) {
    }

     
 
     model = new Organization(0, '', '', null, '', '', '', '', '');

    onSubmit() {
    }
    
     saveOrganization() {
        this.organizationService.saveOrganization(this.model).subscribe(
            data => { 
                this.model = data;
                this.submitted = true;
            },
            
            error =>{ alert(error);}
        );
        
    }
    
    onReset(){
        this.model = new Organization(0, '', '', null, '', '', '', '', '');
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }
    
    goToForm() {
  		this.submitted = false;
  		this.onReset();
  	}
    
   goToList()
   
   {window.location.href = './organizations-list'}
}

