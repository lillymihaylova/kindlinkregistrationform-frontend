"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var organization_service_1 = require('../services/organization.service');
require('../rxjs-extensions');
var OrganizationsListComponent = (function () {
    function OrganizationsListComponent(organizationService) {
        this.organizationService = organizationService;
        this.mode = 'Observable';
    }
    OrganizationsListComponent.prototype.onOrganizationGet = function () {
        var _this = this;
        this.organizationService.onOrganizationGet()
            .subscribe(function (organizations) { return _this.organizations = organizations; }, function (error) { return alert(error); });
    };
    OrganizationsListComponent.prototype.ngOnInit = function () {
        this.onOrganizationGet();
    };
    OrganizationsListComponent.prototype.goBack = function () { window.location.href = './registration-form'; };
    OrganizationsListComponent = __decorate([
        core_1.Component({
            selector: 'organizations-list',
            templateUrl: 'app/components/organizations-list.component.html',
            providers: [organization_service_1.OrganizationService] }), 
        __metadata('design:paramtypes', [organization_service_1.OrganizationService])
    ], OrganizationsListComponent);
    return OrganizationsListComponent;
}());
exports.OrganizationsListComponent = OrganizationsListComponent;
//# sourceMappingURL=organizations-list.component.js.map