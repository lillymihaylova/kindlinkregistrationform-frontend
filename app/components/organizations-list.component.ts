import { Component, OnInit } from '@angular/core';
import { Organization } from '../model/organization';
import { OrganizationService } from '../services/organization.service';
import '../rxjs-extensions';

@Component({
    selector: 'organizations-list',
    templateUrl: 'app/components/organizations-list.component.html',
    providers: [OrganizationService] })

export class OrganizationsListComponent implements OnInit {
    mode = 'Observable';  
    organizations: Organization[];
    
    constructor(private organizationService: OrganizationService){}

    onOrganizationGet() {
        this.organizationService.onOrganizationGet()
        .subscribe(
            organizations => this.organizations = organizations,
            error => alert(error));
    }
    
     ngOnInit() { 
        this.onOrganizationGet(); }
    
     goBack()
      { window.location.href = './registration-form' }
}
