"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var organization_1 = require('../model/organization');
var organization_service_1 = require('../services/organization.service');
var RegistrationFormComponent = (function () {
    function RegistrationFormComponent(organizationService) {
        this.organizationService = organizationService;
        this.active = true;
        this.submitted = false;
        this.model = new organization_1.Organization(0, '', '', null, '', '', '', '', '');
    }
    RegistrationFormComponent.prototype.onSubmit = function () {
    };
    RegistrationFormComponent.prototype.saveOrganization = function () {
        var _this = this;
        this.organizationService.saveOrganization(this.model).subscribe(function (data) {
            _this.model = data;
            _this.submitted = true;
        }, function (error) { alert(error); });
    };
    RegistrationFormComponent.prototype.onReset = function () {
        var _this = this;
        this.model = new organization_1.Organization(0, '', '', null, '', '', '', '', '');
        this.active = false;
        setTimeout(function () { return _this.active = true; }, 0);
    };
    RegistrationFormComponent.prototype.goToForm = function () {
        this.submitted = false;
        this.onReset();
    };
    RegistrationFormComponent.prototype.goToList = function () { window.location.href = './organizations-list'; };
    RegistrationFormComponent = __decorate([
        core_1.Component({
            selector: 'registration-form',
            templateUrl: 'app/components/registration-form.component.html'
        }), 
        __metadata('design:paramtypes', [organization_service_1.OrganizationService])
    ], RegistrationFormComponent);
    return RegistrationFormComponent;
}());
exports.RegistrationFormComponent = RegistrationFormComponent;
//# sourceMappingURL=registration-form.component.js.map