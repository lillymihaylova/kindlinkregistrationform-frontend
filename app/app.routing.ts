import { Routes, RouterModule } from '@angular/router';


import { OrganizationsListComponent } from './components/organizations-list.component';
import { RegistrationFormComponent } from './components/registration-form.component';


const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/registration-form',
        pathMatch: 'full'
    },

    {
        path: 'registration-form',
        component: RegistrationFormComponent
    },

    {
        path: 'organizations-list',
        component: OrganizationsListComponent
    }
    
];

export const routing = RouterModule.forRoot(appRoutes);
