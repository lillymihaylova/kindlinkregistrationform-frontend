import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http'; 

import { AppComponent } from './components/app.component';
import { OrganizationService } from './services/organization.service';
import { OrganizationsListComponent } from './components/organizations-list.component';
import { RegistrationFormComponent } from './components/registration-form.component';
import { routing } from './app.routing';
import 'app/rxjs-extensions';

@NgModule({
  imports: [ 
    BrowserModule,
    FormsModule,
    HttpModule,
    routing 
  ],
  
  declarations: [ 
    AppComponent, 
    OrganizationsListComponent,
    RegistrationFormComponent 
  ],
  providers: [ OrganizationService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
