export class Organization {
    

    constructor(
        public id: number,
        public organizationName: string,
        public registrationNumber: string,
        public dateOfRegistration: string,
        public owner: string,
        public address: string,
        public postCode: string,
        public sphereOfActivity: string,
        public descripntionOfActivity: string
    ) 
    {}
}