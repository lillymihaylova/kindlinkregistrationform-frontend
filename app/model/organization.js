"use strict";
var Organization = (function () {
    function Organization(id, organizationName, registrationNumber, dateOfRegistration, owner, address, postCode, sphereOfActivity, descripntionOfActivity) {
        this.id = id;
        this.organizationName = organizationName;
        this.registrationNumber = registrationNumber;
        this.dateOfRegistration = dateOfRegistration;
        this.owner = owner;
        this.address = address;
        this.postCode = postCode;
        this.sphereOfActivity = sphereOfActivity;
        this.descripntionOfActivity = descripntionOfActivity;
    }
    return Organization;
}());
exports.Organization = Organization;
//# sourceMappingURL=organization.js.map