"use strict";
var router_1 = require('@angular/router');
var organizations_list_component_1 = require('./components/organizations-list.component');
var registration_form_component_1 = require('./components/registration-form.component');
var appRoutes = [
    {
        path: '',
        redirectTo: '/registration-form',
        pathMatch: 'full'
    },
    {
        path: 'registration-form',
        component: registration_form_component_1.RegistrationFormComponent
    },
    {
        path: 'organizations-list',
        component: organizations_list_component_1.OrganizationsListComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map