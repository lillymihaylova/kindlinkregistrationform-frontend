import { Injectable }     from '@angular/core';

import { Organization }   from '../model/organization';
import {Observable} from 'rxjs/Rx';
import { Headers, RequestOptions, Request, RequestMethod, Http, Response } from '@angular/http';

@Injectable()
export class OrganizationService {

    constructor(private http: Http) { }

    private organizationsUrl = 'http://localhost:8083/organizations-list';
    private saveOrganizationUrl = 'http://localhost:8083/registration-form';
    onOrganizationGet(): Observable<Organization[]> {
        return this.http.get(this.organizationsUrl)
            .map(this.extractData)
            .map((organizations: Array<any>) => {
                let result: Array<Organization> = [];
                if (organizations) {
                    organizations.forEach((organization) => {
                        result.push(new Organization(organization.id, organization.organizationName,organization.registrationNumber,organization.dateOfRegistration, organization.owner,
                            organization.address, organization.postCode,
                            organization.sphereOfActivity, organization.descriptionOfActivity));
                            
                    });
                }
                return result;
            });
 }
  private extractData(response: Response) {
        let body = response.json();
        return body;
    }

    saveOrganization(organization): Observable<Organization> {

        let body = JSON.stringify(organization);

        let headers = new Headers({'Content-Type': 'application/json'  });
        let options = new RequestOptions({ headers: headers });


        return this.http.post(this.saveOrganizationUrl, body, options)
            .map((res: Response) => res.json());
            
    }

    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); 
        return Observable.throw(errMsg);;
    }
}

